package oop.concur1.chrono;

public class ChronoController implements Controller {

	private ChronoAgent agent;
	private Counter counter;
	private ChronoView view;
	
	public ChronoController(Counter counter) {
		this.counter = counter;
	}
	
	public void attachView(ChronoView view) {
		this.view = view;
	}
	
	@Override
	public void notifyStarted() {
		agent = new ChronoAgent(counter, view);
		agent.start();
	}

	@Override
	public void notifyStopped() {
		agent.notifyStopped();
	}

	@Override
	public void notifyReset() {
		counter.reset();
		view.updateCount(counter.getValue());
	}
}
