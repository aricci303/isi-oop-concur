package oop.concur1.chrono;

public class Counter {

	private int cont;
	private int base;

	public Counter(int base) {
		this.cont = base;
		this.base = base;
	}

	public void inc() {
		cont++;
		System.out.println("count " + cont);
	}

	public void reset() {
		cont = base;
	}

	public int getValue() {
		return cont;
	}
}
