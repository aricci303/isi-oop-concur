package oop.concur1.chrono;

public class TestChrono {
	public static void main(String[] args) {

		Counter c = new Counter(0);
		Controller controller = new ChronoController(c);
		ChronoView view = new ChronoView(c.getValue(), controller);
		controller.attachView(view);
		view.show();

	}
}
