package oop.concur1.bouncingballs;

/**
 * SISOP 08/09
 *
 * @author Alessandro Ricci
 */
public class BouncingBalls {

	public static void main(String[] args) {

		Context ctx = new Context();

		Visualiser viewer = new Visualiser(ctx);
		viewer.start();

		ControlPanel control = new ControlPanel(ctx);
		control.setVisible(true);
	}
}
