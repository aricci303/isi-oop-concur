package oop.concur2;

public interface Counter {

	void inc();

	void dec();

	long getValue();
}
